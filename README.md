# rvbb

Un script Python pour colorer un texte d'après une image. Résultat sous la forme d'une page html.

## Système

- Linux
- OSX

## Dépendances

- Python 3
- [PIL](https://pillow.readthedocs.io/)

## Comment lancer le script ?

- Dans le dossier où se trouve le script, glisser l'image de référence et le fichier texte (au format `.txt`)
- Ouvrir le terminal
- Ouvrir le dossier contenant le script : `cd chemin/du/dossier`
- Lancer le script (avec en arguments l'image de référence, puis le fichier texte) : `python3 rvbb.py nom_de_l_image.format nom_du_fichier_texte.txt` (par exemple, `python3 rvbb.py img.jpg texte.txt`)
- Un fichier `index.html` sera généré, à ouvrir dans votre navigateur !

## Paramètres
- Pour modifier le style (css) du texte généré, il suffit de changer la variable `style` dans le script

## Résultats

(Texte issu du [Manifeste du sabotage en entreprises](https://www.infokiosques.net/spip.php?article479))

![Avant](exemples/bill_avant.jpg)
-> -> -> 
![Après](exemples/bill_apres.png)


![Avant](exemples/casserole_avant.jpeg)
-> -> -> 
![Après](exemples/casserole_apres.png)




